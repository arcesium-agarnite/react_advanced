import * as React from "react";
import { ProductItem } from "../productItem/product-item";

export const ProductList = (props) => {
  const { products, loading } = props;

  return (
    <div className="grid grid-cols-1 gap-x-6 gap-y-10 sm:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4 xl:gap-x-8">
      {loading ? (
        <h1>Products are loading...</h1>
      ) : products.length > 0 ? (
        products.map((product) => (
          <ProductItem product={product} key={product.id} />
        ))
      ) : (
        <h1>Products are empty</h1>
      )}
    </div>
  );
};
