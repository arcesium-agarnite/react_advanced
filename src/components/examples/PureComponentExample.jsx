import * as React from 'react';

const PureComponentExample = () => {
    const [count, setCount] = React.useState(0);
    const [todos, setTodos] = React.useState(["todo 1", "todo 2"]);

    const increment = () => {
        setCount((c) => c + 1);
    };
    return (<div className='mt-20 ml-10'>
        <div className='font-semibold'>Pure Component Example</div>
        <Todos todos={todos} />
        <hr />
        <div>
            Count: {count}
            <button className='px-4 text-white bg-blue-600 border-l' onClick={increment}>+</button>
        </div>
    </div>);
};


const Todos = React.memo(({ todos }) => {
    console.log("child render");
    return (
        <>
            <h2>My Todos</h2>
            {todos.map((todo, index) => {
                return <p key={index}>{todo}</p>;
            })}
        </>
    );
});

export default PureComponentExample;
