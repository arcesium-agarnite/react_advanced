import * as React from 'react';
import { useRef } from "react";

const UncontrolledForm = () => {
    const selectRef = useRef(null);
    const checkboxRef = useRef(null);
    const inputRef = useRef(null);

    function handleSubmit(event) {
        event.preventDefault();        
        console.log("Input value:", inputRef.current.value);
        console.log("Select value:", selectRef.current.value);
        console.log("Checkbox value:", checkboxRef.current.checked);
    }

    return (
        <div className='mt-20 ml-10'>
            <div className='font-semibold'>UnControlled Form</div>
            <form onSubmit={handleSubmit}>
                <div>
                    <label>
                        <p>Name:</p>
                        <input className='border-2' ref={inputRef} type="text" />
                    </label>
                </div>
                <div>
                    <label>
                        <p>Favorite color:</p>
                        <select className='border-2' ref={selectRef}>
                            <option value="red">Red</option>
                            <option value="green">Green</option>
                            <option value="blue">Blue</option>
                        </select>
                    </label>
                </div>
                <div>
                    <label>
                        Do you like React?
                        <input className='border-2' type="checkbox" ref={checkboxRef} />
                    </label>
                </div>
                <button className='px-4 text-white bg-blue-600 border-l' type="submit">Submit</button>
            </form>
        </div>
    );
}

export default UncontrolledForm;