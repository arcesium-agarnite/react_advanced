import * as React from 'react';
import { forwardRef } from 'react';

const ForwardRefExample = () => {
    const ref = React.useRef(null);

    function handleClick() {
        ref.current.focus();
    }

    React.useEffect(() => {
        setTimeout(() => {
            ref.current.value = "from parent...";
        },2000)
    },[])

    return (
        <div className='mt-20 ml-10'>
            <div className='font-semibold'>Forward Ref Example</div>
            <form>
                <MyInput label="Enter your name:" ref={ref} />
                <button className='px-4 text-white bg-blue-600 border-l' type="button" onClick={handleClick}>
                    Edit
                </button>
            </form>
        </div>
    );
}

const MyInput = forwardRef(function MyInput(props, ref) {
    const { label, ...otherProps } = props;
    return (
        <label>
            {label}
            <input className='border-2' {...otherProps} ref={ref} />
        </label>
    );
});

export default ForwardRefExample;