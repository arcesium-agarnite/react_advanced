import * as React from 'react';

const UseMemoExample = () => {

    const [count, setCount] = React.useState(0);
    const [todos, setTodos] = React.useState([]);
    const calculation = React.useMemo(() => expensiveCalculation(count), [count]);

    const increment = () => {
        setCount((c) => c + 1);
    };
    const addTodo = () => {
        setTodos((t) => [...t, "New Todo"]);
    };

    return (
        <div className='mt-20 ml-10'>
            <div className='font-semibold'>useMemo Example</div>
            <div>
                <h2>My Todos</h2>
                {todos.map((todo, index) => {
                    return <p key={index}>{todo}</p>;
                })}
                <button className='px-4 text-white bg-blue-600 border-l' onClick={addTodo}>Add Todo</button>
            </div>
            <hr />
            <div>
                Count: {count}
                <button className='px-4 text-white bg-blue-600 border-l' onClick={increment}>+</button>
                <h2>Expensive Calculation</h2>
                {calculation}
            </div>
        </div>
    );
};

const expensiveCalculation = (num) => {
    console.log("Calculating...");
    for (let i = 0; i < 1000; i++) {
        num += 1;
    }
    return num;
};

export default UseMemoExample;