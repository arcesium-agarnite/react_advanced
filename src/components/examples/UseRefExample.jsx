import * as React from 'react';

const UseRefExample = () => {
    const count = React.useRef(0);

    // const inputRef = React.useRef("")

    // return(
    //     <input type="text" ref={inputRef} />
    // )

    return (
        <div className='mt-20 ml-10'>
            <div className='font-semibold'>UseRef Example</div>
            <input
                className='border-2'
                type="text"
                onChange={(e) => {count.current = Number(e.target.value)}}
            />
            <button className='px-4 text-white bg-blue-600 border-l' onClick={()=>{alert(count.current*count.current)}}>Display Square of Number</button>            
        </div>
    );
}

export default UseRefExample;
