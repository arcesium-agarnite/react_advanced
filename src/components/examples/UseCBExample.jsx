import * as React from 'react';

const UseCBExample = () => {
    const [count, setCount] = React.useState(0);
    const [todos, setTodos] = React.useState([]);

    const increment = () => {
        setCount((c) => c + 1);
    };
    const addTodo = React.useCallback(() => {
        setTodos((t) => [...t, "New Todo"]);
    }, [todos]);

    return (
        <div className='mt-20 ml-10'>
            <div className='font-semibold'>Use callback Example</div>
            <TodosCB todos={todos} addTodo={addTodo} />
            <hr />
            <div>
                Count: {count}
                <button className='px-4 text-white bg-blue-600 border-l' onClick={increment}>+</button>
            </div>
        </div>
    );
};

const TodosCB = ({ todos, addTodo }) => {
    console.log("child render");
    return (
        <>
            <h2>My Todos</h2>
            {todos.map((todo, index) => {
                return <p key={index}>{todo}</p>;
            })}
            <button className='px-4 text-white bg-blue-600 border-l' onClick={addTodo}>Add Todo</button>
        </>
    );
};

export default UseCBExample;
