import * as React from 'react';

const KeyExample = () => {
    const [fruits, setFruits] = React.useState(["Papaya", "Banana", "Apple", "Mango"]);

    const addFruit = () => {
        setFruits((prevList) => {
            return [...prevList, ...["Orange", "Grapes"]]
        })
    }

    const addFruitAtTop = () => {
        setFruits((prevList) => {
            return [...["Orange", "Grapes"], ...prevList]
        })
    }

    return (<div className='mt-20 ml-10'>
        <div className='font-semibold'>Key Example</div>
        <ul>
            {
                fruits.map((fruit, index) => {
                    return <li key={fruit}>{fruit}</li>
                })
            }
        </ul>
        <div>
            <button className='px-4 text-white bg-blue-600 border-l my-2' onClick={addFruit}>Add Fruit At Last</button>
        </div>
        <div>
            <button className='px-4 text-white bg-blue-600 border-l' onClick={addFruitAtTop}>Add Fruit At Top</button>
        </div>
    </div>);
}

export default KeyExample;