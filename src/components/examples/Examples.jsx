import * as React from 'react';
import KeyExample from "./KeyExample";
import PureComponentExample from "./PureComponentExample";
import UseMemoExample from './UseMemoExample';
import UseCBExample from './UseCBExample';
import UseReducerExample from './UseReducerExample';
import UseRefExample from './UseRefExample';
import ForwardRefExample from './ForwardRefExample';
import ControlledForm from './ControlledForm';
import UncontrolledForm from './UncontrolledForm';

const Examples = () => {
    return (
        <>
            <KeyExample />
            <PureComponentExample />
            <UseMemoExample />
            <UseCBExample />
            <UseReducerExample />
            <UseRefExample />
            <ForwardRefExample />
            <ControlledForm />
            <UncontrolledForm />
        </>
    )
}




export default Examples;