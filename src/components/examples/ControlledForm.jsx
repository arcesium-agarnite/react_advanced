import * as React from 'react';

import { useState } from 'react';

const ControlledForm = () => {
    const [inputValue, setInputValue] = useState('');
    const [inputError, setInputError] = useState(null);

    function handleInputChange(event) {
        const value = event.target.value;
        setInputValue(value);

        if (value.length < 5) {
            setInputError('Input must be at least 5 characters');
        } else {
            setInputError(null);
        }
    }

    function handleSubmit(event) {
        event.preventDefault();
        if (inputValue.length >= 5) {
            alert('submitting form... value' + inputValue);
        } else {
            setInputError('Input must be at least 5 characters');
        }
    }

    return (
        <div className='mt-20 ml-10'>
            <div className='font-semibold'>Controlled Form</div>
            <form onSubmit={handleSubmit}>
                <label>
                    Enter Input:
                    <input className='border-2' type="text" value={inputValue} onChange={handleInputChange} />
                </label>
                {inputError && <div style={{ color: 'red' }}>{inputError}</div>}
                <button className='px-4 text-white bg-blue-600 border-l' type="submit">Submit</button>
            </form>
        </div>
    );
}

export default ControlledForm;
