import * as React from "react";
import { useContext } from "react";
import { StoreContext } from "../../storeContext";

const ProductsCart = () => {
    const { storeState } = useContext(StoreContext);
    const [total, setTotal] = React.useState(0);
    
    React.useEffect(() => {
        let tempTotal = 0;
        storeState.forEach(({price})=>{
            tempTotal = tempTotal + Number(price);
        })
        setTotal(tempTotal);
    },[storeState])

    return (
        <div style={{width:'15vw', height:'80vh'}} className="flex flex-col items-center justify-center bg-slate-100">
            Hello
            {
                storeState.length
                    ?
                    storeState.map(({ title, price }) => {
                        return (
                            <div key={title} className="flex flex-wrap justify-center items-center border-b-2">
                                <div className="w-full"></div>{title}<div className="font-bold w-full">{price}</div>                                
                            </div>
                        )
                    })
                    :
                    "Please add items :)"
            }
            {total ? <div className="font-bold">Total: {total}</div>:""}
        </div>
    );
}

export default ProductsCart;