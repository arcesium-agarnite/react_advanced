import * as React from "react";
import { useContext } from "react";
import { StoreContext } from "../../storeContext";

export const ProductItem = (props) => {
  const { product } = props;
  const [isAdded, setAdded] = React.useState(false);
  const {storeState, setStoreState} = useContext(StoreContext);

  const manageProductinCart = (shouldAdd) => {
    let tempState = []
    if(shouldAdd) {
      const productData = {
        title:product.title, 
        price: product.price
      }
      tempState = [...storeState, productData];
    }else {
      tempState = storeState.filter((productItem) => {
        return productItem.title !== product.title
      });
    }
    setStoreState(tempState);
    setAdded(shouldAdd)
  };

  return (
    <div className="group cursor-pointer">
      <div className="aspect-h-1 aspect-w-1 w-full overflow-hidden rounded-lg bg-gray-200 xl:aspect-h-8 xl:aspect-w-7">
        <img
          alt={product.title}
          src={product.image}
          loading="lazy"
          className="h-80 w-full object-cover object-center group-hover:opacity-75"
        />
      </div>
      <h3 className="mt-4 text-sm text-gray-700 overflow-hidden truncate">
        {product.title}
      </h3>
      <div className="flex justify-between">
        <p className="mt-1 text-lg font-medium text-gray-900">${product.price}</p>
        {!isAdded && <button className="px-4 text-white bg-blue-600 border-l" onClick={()=>{manageProductinCart(true)}}>Add</button>}
        {isAdded && <button className="px-4 text-white bg-red-600 border-l" onClick={()=>{manageProductinCart(false)}}>Remove</button>}        
      </div>
    </div>
  );
};
