import * as React from "react";

export const SearchBar = (props) => {
  const { onSearch } = props;
  const [text, setText] = React.useState("");

  const handleChange = (e) => {
    setText(e.target.value);
  };

  const handleClick = () => {
    onSearch(text);
  };

  return (
    <div data-testid="searchBar" className="flex mb-10 mt-8">
      <div className="flex border-2 border-gray-200 rounded w-full justify-between">
        <input
          type="text"
          className="px-4 py-3 w-5/6 focus:outline-none focus:ring-1"
          placeholder="Search your product here..."
          value={text}
          onChange={handleChange}
        />
        <button
          className="px-4 text-white bg-blue-600 border-l w-1/6"
          onClick={handleClick}
        >
          Search
        </button>
      </div>
    </div>
  );
};
