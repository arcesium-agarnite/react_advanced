import * as React from "react";
import { useContext } from "react";
import { StoreContext } from "../../storeContext";
import { Link as RouterLink, useLocation } from 'react-router-dom';

const navRoutes = [
  {
    label: 'All',
    route: '/'
  },
  {
    label: 'Electronics',
    route: '/electronics'
  },
  {
    label: 'Jewellery',
    route: '/jewellery'
  },
  {
    label: 'Men\'s Clothing',
    route: 'men-clothing'
  },
  {
    label: 'Women\'s Clothing',
    route: 'women-clothing'
  }
]

export const Navigation = () => {
  const [active, setActive] = React.useState(0);
  const { storeState } = useContext(StoreContext);

  const navigationStyle = "rounded-md px-3 py-2 text-sm font-medium";

  return (
    <nav className="bg-gray-800 fixed w-full top-0">
      <div className="mx-auto max-w-7xl px-2 sm:px-6 lg:px-8">
        <div className="relative flex h-16 items-center justify-between">
          <div className="flex flex-1 items-center justify-center sm:items-stretch sm:justify-start">
            <div className="flex flex-shrink-0 items-center">
              <img
                className="h-8 w-auto"
                src="https://tailwindui.com/img/logos/mark.svg?color=indigo&shade=500"
                alt="Your Company"
              />
            </div>
            <div className="hidden sm:ml-6 sm:block">
              <div className="flex space-x-4">
                {navRoutes.map((nav, index) => (
                  <RouterLink
                    key={nav.route}
                    component={RouterLink}
                    to={nav.route}
                    underline="none"
                    onClick={() => setActive(index)}
                    className={
                      active === index
                        ? `${navigationStyle} bg-gray-900 text-white`
                        : `${navigationStyle} text-gray-300 hover:bg-gray-700 hover:text-white`
                    }
                  >
                    {nav.label}
                  </RouterLink>
                ))}
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="text-white absolute top-4 right-[4%] bg-red-400 w-8 h-8 rounded-full flex items-center justify-center">
        {storeState.length}
      </div>
    </nav>
  );
};
