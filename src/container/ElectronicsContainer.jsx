import * as React from "react";
import { ProductList } from "../components/productList/product-list";
import { SearchBar } from "../components/searchBar/search-bar";

const ElectronicsContainer = () => {
    const [products, setProducts] = React.useState([]);
    const [loading, setLoading] = React.useState(true);
    const [searchText, setSearchText] = React.useState("");

    React.useEffect(() => {
        const fetchProducts = async () => {
            const response = await fetch("https://fakestoreapi.com/products/category/electronics");
            const data = await response.json();
            setProducts(data);
            setLoading(false);
        };
        fetchProducts();
    }, []);

    const filteredProducts = products.filter((product) => {
        return product?.title
            .split(" ")
            .some((word) =>
                word.toLowerCase().startsWith(searchText?.toLocaleLowerCase())
            );
    });

    return (
        <>
            <div className="mx-auto max-w-2xl px-4 py-16 sm:px-6 sm:py-24 lg:max-w-7xl lg:px-8">
                <h2 className="category">Store</h2>
                <SearchBar onSearch={setSearchText} />
                <ProductList products={filteredProducts} loading={loading} />
            </div>
        </>
    );
}

export default ElectronicsContainer;