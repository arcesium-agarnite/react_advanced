import * as React from "react";
import { StoreContext } from "./storeContext";
import ProductsCart from "./components/productsCart/products-cart";
import { Navigation } from "./components/navigation/navigation";
import { Route, Routes, useLocation } from 'react-router-dom';
// import HomeContainer from './container/HomeContainer';
// import MenClothContainer from './container/MenClothContainer';
// import ElectronicsContainer from './container/ElectronicsContainer';
import Examples from './components/examples/Examples';
import "./app.css";

const HomeContainer = React.lazy(() => import('./container/HomeContainer'));
const MenClothContainer = React.lazy(() => import('./container/MenClothContainer'));
const ElectronicsContainer = React.lazy(() => import('./container/ElectronicsContainer'));

function App() {
  const [storeState, setStoreState] = React.useState([]);

  return (
    <StoreContext.Provider value={{ storeState, setStoreState }}>
      <div className="bg-white">
        <Navigation />
        <div style={{ width: '15vw' }} className="fixed bottom-0 right-10 text-center">
          <div className="text-gray-600 underline">Cart</div>
          <div>
            <ProductsCart />
          </div>
        </div>
        <div style={{ width: '80vw' }}>
          <React.Suspense fallback={"loading..."}>
            <Routes>
              <Route path="men-clothing" element={<MenClothContainer />} />
              <Route path="electronics" element={<ElectronicsContainer />} />
              <Route path="examples" element={<Examples />} />
              <Route exact path="/" element={<HomeContainer />} />
            </Routes>
          </React.Suspense>
        </div>
      </div>
    </StoreContext.Provider>
  );
}

export default App;

