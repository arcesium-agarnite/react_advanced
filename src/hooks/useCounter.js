import React from "react"

export const useCounter = () => {
    const [currentCount, setCurretCount] = React.useState(0);
    const increaseCount = () => {
        setCurretCount((prevCount)=>{
            return prevCount + 1
        })
    }
    const decreaseCount = () => {
        setCurretCount((prevCount)=>{
            return prevCount - 1
        })
    }
    return {count: currentCount, increaseCount, decreaseCount}
}