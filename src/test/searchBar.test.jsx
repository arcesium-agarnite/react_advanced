import React from 'react';
import { fireEvent, render, screen, waitFor } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { SearchBar } from "../search-bar";

describe('Searchbar different test cases', () => {
    it('searchbar should exist', () => {
        render(<SearchBar onSearch={()=>{}} />)
        const searchElement = screen.getByTestId('searchBar');
        expect(searchElement).toBeInTheDocument();
    })

    it('searchbar should change input value', async () => {
        render(<SearchBar onSearch={()=>{}} />)
        const inputElement = screen.getByRole('textbox');
        expect(inputElement).toBeInTheDocument();
        await userEvent.type(inputElement,"jacket");
        expect(inputElement).toHaveValue("jacket");
    })
    
    it('btn click should call handler', async () => {
        const searchHandler = jest.fn();
        render(<SearchBar onSearch={searchHandler} />)
        const btnElement = screen.getByRole('button',{name:'Search'});
        expect(btnElement).toBeInTheDocument();
        await userEvent.click(btnElement);
        expect(searchHandler).toHaveBeenCalledTimes(1);
    })    
})